# Projeto 4
Matheus Sanstos Carvalho
Sistemas de Informação - INF -UFG
Aplicações Distribuídas

# Passo a passo
1 - Abrir o diretório principal do projeto.<br>
2 - Clicar com o botão direito no arquivo chat-video.html.<br>
3 - Clicar em **abrir com**.<br>
4 - Escolher o navegador de sua preferência.<br>
5 - Permitir o acesso à câmera e ao áudio.<br>
6 - Informar o ID do peer que deseja conectar.<br>
7 - Clicar em Call.<br>
8 - Finalizar a chamada.<br>

# Explicação do Código

O código é composto por três aquivos principais sendo um do tipo JavaScript contendo as funções relacionadas à arquitetura P2P (peerFunctions.js), um
arquiv HTML contendo a marcação da página (chat-video.html) e um arquivo de estilização (style.css).<p>
Os de maior importância para o trabalho são os arquivos chat-video.html e peerFunctions.js.

# Arquivo HTML

Para utilização da biblioteca PeerJS é necessário importá-la adicionando a seguinte linha no cabeçalho do documento HTML principal:<br>

``<script src="https://cdnjs.cloudflare.com/ajax/libs/peerjs/0.3.9/peer.min.js"></script>``

# JavaScript

**1 - Criação do Objeto Peer**

O objeto Peer é onde criamos e recebemos conexões.<br>
Com a inserção da linha abixo é o objeto Peer, que quando conectado terá seu ID para que outros peers consigam conectá-lo.<br>


<code> var peer = new Peer({debug: 3});<code>

**2 - Geração do ID**

Cada objeto Peer tem um ID único e aleatório quando é criado.<br>
O comando **open** conecta o peer a uma rede P2P e retorna para ele um ID.<br>

<code>peer.on('open', function(id) { $('#my-id').text(id);});<code>

**3 - Realização da Chamada de Vídeo**

O evento **call** capta conexões de chamada de vídeo a um determinado peer e tem uma função de callback que retorna a chamada/conexão.<br>

``peer.on('call', function(call) {
    call.answer(window.localStream);
    step3(call);
});``

A função **answer** server para aceitar ou recusar uma solitação de conexão de chamada de vídeo. Neste exemplo toda chamada é aceita automaticamante.

**4 - Realizar Chamada**

Para realizar uma chamada o peer deve informar seu ID e passar as permissões de acesso ao seu hardware (vídeo e áudio) permitindo a comunicação em tempo real.<br>

``var call = peer.call($('#callto-id').val(), window.localStream);``

# Referências
https://github.com/peers/peerjs/tree/master/examples <br>
https://peerjs.com/

